<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class VehicleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //$this->faker->randomElement($intervals)
            'year' => $this->faker->randomElement([2010, 2012, 2014, 2016, 2018, 2020]),
            'make' => $this->faker->randomElement(['Honda', 'Toyota', 'Ford', 'Chevrolet']),
            'model' => $this->faker->randomElement(['CRV', 'Corolla', 'Focus', 'Traverse']),
            'vin' => Str::random(10)
        ];
    }
}
