<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderStoreRequest;
use App\Models\Order;
use App\Models\Key;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Order $order)
    {
        $orders = $order->newQuery();

        if ($request->input('key_id')) {
            $orders->where('key_id', '=', $request->input('key_id'));
        }
        if ($request->input('vehicle_id')) {
            $orders->where('vehicle_id', '=', $request->input('vehicle_id'));
        }
        if ($request->input('technician_id')) {
            $orders->where('technician_id', '=', $request->input('technician_id'));
        }

        // $orders->with([
        //     'key' => function ($q) {
        //         $q->select(['id', 'name']);
        //     },
        //     'vehicle' => function ($q) {
        //         $q->select(['id', 'year', 'make', 'model', 'vin']);
        //     },
        //     'technician' => function($q) {
        //         $q->select(['id', 'first_name', 'last_name', 'truck_number']);
        //     }
        // ]);
        return $orders->get()->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderStoreRequest $request)
    {
        $data = $request->all();
        $item = Key::find($request->input('key_id'));
        $data['price'] = $item->price * $data['amount'];

        $order = Order::create($data);

        return response()->json($order);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        return response()->json($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderStoreRequest $request, $id)
    {
        $data = $request->all();
        $item = Key::find($request->input('key_id'));
        $data['price'] = $item->price * $data['amount'];
        
        $order = Order::find($id);
        $order->update($data);

        return response()->json($order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();

        return response()->json('Order deleted!');
    }
}
